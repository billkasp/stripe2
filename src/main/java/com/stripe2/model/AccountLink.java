package com.stripe2.model;

import com.google.gson.annotations.SerializedName;
import com.stripe2.Stripe;
import com.stripe2.exception.StripeException;
import com.stripe2.net.ApiResource;
import com.stripe2.net.RequestOptions;
import com.stripe2.param.AccountLinkCreateParams;
import java.util.Map;
import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
@EqualsAndHashCode(callSuper = false)
public class AccountLink extends ApiResource {
  /** Time at which the object was created. Measured in seconds since the Unix epoch. */
  @SerializedName("created")
  Long created;

  /** The timestamp at which this account link will expire. */
  @SerializedName("expires_at")
  Long expiresAt;

  /** String representing the object's type. Objects of the same type share the same value. */
  @SerializedName("object")
  String object;

  /** The URL for the account link. */
  @SerializedName("url")
  String url;

  /**
   * Creates an AccountLink object that returns a Stripe URL that the user can redirect their user
   * to in order to take them through the hosted onboarding flow.
   */
  public static AccountLink create(Map<String, Object> params) throws StripeException {
    return create(params, (RequestOptions) null);
  }

  /**
   * Creates an AccountLink object that returns a Stripe URL that the user can redirect their user
   * to in order to take them through the hosted onboarding flow.
   */
  public static AccountLink create(Map<String, Object> params, RequestOptions options)
      throws StripeException {
    String url = String.format("%s%s", Stripe.getApiBase(), "/v1/account_links");
    return ApiResource.request(
        ApiResource.RequestMethod.POST, url, params, AccountLink.class, options);
  }

  /**
   * Creates an AccountLink object that returns a Stripe URL that the user can redirect their user
   * to in order to take them through the hosted onboarding flow.
   */
  public static AccountLink create(AccountLinkCreateParams params) throws StripeException {
    return create(params, (RequestOptions) null);
  }

  /**
   * Creates an AccountLink object that returns a Stripe URL that the user can redirect their user
   * to in order to take them through the hosted onboarding flow.
   */
  public static AccountLink create(AccountLinkCreateParams params, RequestOptions options)
      throws StripeException {
    String url = String.format("%s%s", Stripe.getApiBase(), "/v1/account_links");
    return ApiResource.request(
        ApiResource.RequestMethod.POST, url, params, AccountLink.class, options);
  }
}
