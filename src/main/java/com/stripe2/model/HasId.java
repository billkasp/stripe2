package com.stripe2.model;

public interface HasId {
  String getId();
}
