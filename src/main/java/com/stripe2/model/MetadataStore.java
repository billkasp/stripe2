package com.stripe2.model;

import com.stripe2.exception.StripeException;
import com.stripe2.net.RequestOptions;
import java.util.Map;

/** Common interface for Stripe objects that can store metadata. */
public interface MetadataStore<T> {
  Map<String, String> getMetadata();

  MetadataStore<T> update(Map<String, Object> params) throws StripeException;

  MetadataStore<T> update(Map<String, Object> params, RequestOptions options)
      throws StripeException;
}
