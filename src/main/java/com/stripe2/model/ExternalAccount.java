package com.stripe2.model;

import com.stripe2.exception.StripeException;
import com.stripe2.net.RequestOptions;
import java.util.Map;

public interface ExternalAccount extends HasId {
  ExternalAccount update(Map<String, Object> params, RequestOptions options) throws StripeException;

  ExternalAccount update(Map<String, Object> params) throws StripeException;

  ExternalAccount delete(Map<String, Object> params, RequestOptions options) throws StripeException;

  ExternalAccount delete() throws StripeException;

  ExternalAccount delete(RequestOptions options) throws StripeException;

  ExternalAccount delete(Map<String, Object> params) throws StripeException;
}
