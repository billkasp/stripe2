package com.stripe2.model.oauth;

import com.stripe2.model.StripeObject;
import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
@EqualsAndHashCode(callSuper = false)
public class OAuthError extends StripeObject {
  String error;
  String errorDescription;
}
