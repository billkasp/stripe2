package com.stripe2.model.reporting;

import com.stripe2.model.StripeCollection;

public class ReportRunCollection extends StripeCollection<ReportRun> {}
