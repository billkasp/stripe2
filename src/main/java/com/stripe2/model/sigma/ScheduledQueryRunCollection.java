package com.stripe2.model.sigma;

import com.stripe2.model.StripeCollection;

public class ScheduledQueryRunCollection extends StripeCollection<ScheduledQueryRun> {}
