package com.stripe2.model;

import com.stripe2.Stripe;
import com.stripe2.exception.StripeException;
import com.stripe2.net.ApiResource;
import com.stripe2.net.RequestOptions;
import com.stripe2.param.BitcoinTransactionCollectionListParams;
import java.util.Map;

public class BitcoinTransactionCollection extends StripeCollection<BitcoinTransaction> {
  /** List bitcoin transacitons for a given receiver. */
  public BitcoinTransactionCollection list(Map<String, Object> params) throws StripeException {
    return list(params, (RequestOptions) null);
  }

  /** List bitcoin transacitons for a given receiver. */
  public BitcoinTransactionCollection list(Map<String, Object> params, RequestOptions options)
      throws StripeException {
    String url = String.format("%s%s", Stripe.getApiBase(), this.getUrl());
    return ApiResource.requestCollection(url, params, BitcoinTransactionCollection.class, options);
  }

  /** List bitcoin transacitons for a given receiver. */
  public BitcoinTransactionCollection list(BitcoinTransactionCollectionListParams params)
      throws StripeException {
    return list(params, (RequestOptions) null);
  }

  /** List bitcoin transacitons for a given receiver. */
  public BitcoinTransactionCollection list(
      BitcoinTransactionCollectionListParams params, RequestOptions options)
      throws StripeException {
    String url = String.format("%s%s", Stripe.getApiBase(), this.getUrl());
    return ApiResource.requestCollection(url, params, BitcoinTransactionCollection.class, options);
  }
}
