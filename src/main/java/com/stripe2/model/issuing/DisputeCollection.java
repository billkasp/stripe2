package com.stripe2.model.issuing;

import com.stripe2.model.StripeCollection;

public class DisputeCollection extends StripeCollection<Dispute> {}
