package com.stripe2.model.issuing;

import com.stripe2.model.StripeCollection;

public class TransactionCollection extends StripeCollection<Transaction> {}
