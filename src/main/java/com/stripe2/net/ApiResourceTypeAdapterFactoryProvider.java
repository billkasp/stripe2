package com.stripe2.net;

import com.google.gson.TypeAdapterFactory;
import com.stripe2.model.BalanceTransactionSourceTypeAdapterFactory;
import com.stripe2.model.ExternalAccountTypeAdapterFactory;
import com.stripe2.model.PaymentSourceTypeAdapterFactory;
import java.util.ArrayList;
import java.util.List;

/**
 * Provider for all {@link TypeAdapterFactory} required for deserializing subtypes of an interface.
 */
final class ApiResourceTypeAdapterFactoryProvider {
  private static final List<TypeAdapterFactory> factories = new ArrayList<>();

  static {
    factories.add(new BalanceTransactionSourceTypeAdapterFactory());
    factories.add(new ExternalAccountTypeAdapterFactory());
    factories.add(new PaymentSourceTypeAdapterFactory());
  }

  public static List<TypeAdapterFactory> getAll() {
    return factories;
  }
}
